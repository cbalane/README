## Chris Balane's README

### My role at GitLab
Principal Product Manager, GitLab Dedicated

Previously:
- Senior Product Manager, GitLab Dedicated
- Senior Product Manager, US Public Sector Services (and also supported the Dedicated Team and SaaS Platforms)
- Senior Product Manager, Release Stage

### Who I am
- Chris is my preferred nickname.
- I was born in New York and have lived in the US for my entire life. I currently reside in Washington, DC.
- I am Filipino
- My academic background is in Computer Science. I have a Bachelor of Science degree from Stanford University and a Master of Science degree from the Johns Hopkins University.
- Music is my first passion and lifetime hobby. I love nearly all forms of music and have spent my whole life learning to play musicical instruments. My main instruments include piano, electric and acoustic guitar, and electric bass.

### How I work
- Timezone is ET; hours are usually 1000-1800 and I take a break at 1200 to work out and eat lunch. I mostly work regular hours but will sometimes take advantage of GitLab’s support for non-linear workdays.
- I use TODOs and issues as much as I can but Slack DM can be good for urgent things
- I am mission and outcome driven
- I love learning and am very curious about most things
- I don’t shy away from very difficult challenges or problems and will aim to solve them myself; I’m learning how to better communicate this upwards. This may result in me usually saying things are good
- I do not like to be micro-managed and work best in an empowered team when I have a sense of ownership and direct responsibility for my scope
- I appreciate process when it is well-thought out and contributes to efficiency and quality, but not process for the sake of process.
- 16personalities: ENTJ-A
- Insights Discovery: Reforming Director
